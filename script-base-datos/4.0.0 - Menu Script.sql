use db_buenas_rutas;
-- ---------------------------------------------- 
-- LISTA MENU DEL SISTEMA POR SESION USUARIO
-- ----------------------------------------------
DROP PROCEDURE IF EXISTS SP_MNU_LST_CFG;
DELIMITER $$
CREATE PROCEDURE SP_MNU_LST_CFG(
	IN  p_token 	VARCHAR(255),	-- TOKEN USER
	OUT MSG_EXPIRE 	VARCHAR(255)	-- EXPIRATION MESSAGE
)
BEGIN	
	DECLARE P_ID_USR_SSN	INT;
	SELECT FN_IS_USER_AUTHORIZED(p_token) INTO P_ID_USR_SSN;
	IF P_ID_USR_SSN  = 1 THEN
		SELECT ID_USR INTO P_ID_USR_SSN FROM cfg_acc_sys WHERE ACCESS_TOKEN = p_token;
		SELECT 
		m.id_mnu 		id,
		m.name			name,
		m.url			url,
		m.icon 			icon
		FROM cfg_mnu m, sys_mnu_rol mr
		where 
		m.id_mnu = mr.id_mnu and 
		mr.id_rol = P_ID_USR_SSN;
	ELSEIF P_ID_USR_SSN = -1 THEN
		SELECT VAL_2 INTO MSG_EXPIRE FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_TOKEN_NO_VALID';
	ELSEIF P_ID_USR_SSN	= 0 THEN
		SELECT VAL_2 INTO MSG_EXPIRE FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
	END IF;
END$$
DELIMITER ;
-- ---------------------------------------------- 
-- LISTA SUB MENUS DEL SISTEMA POR MENU
-- ----------------------------------------------
DROP PROCEDURE IF EXISTS SP_SUB_MNU_LST_CFG;
DELIMITER $$
CREATE PROCEDURE SP_SUB_MNU_LST_CFG(
	IN  p_token 	VARCHAR(255),	-- TOKEN USER
	IN  p_menu 		int,			-- ID MENU
	OUT MSG_EXPIRE 	VARCHAR(255)	-- EXPIRATION MESSAGE
)
BEGIN	
	DECLARE P_ID_USR_SSN	INT;
	SELECT FN_IS_USER_AUTHORIZED(p_token) INTO P_ID_USR_SSN;
	IF P_ID_USR_SSN  = 1 THEN
		SELECT 
		sm.id_mnu 		id,
		sm.id_sub_mnu 	id_sub,
		sm.name			name,
		sm.url			url,
		sm.icon 		icon
		FROM cfg_sub_mnu sm
		where 
		sm.id_mnu = p_menu;
	ELSEIF P_ID_USR_SSN = -1 THEN
		SELECT VAL_2 INTO MSG_EXPIRE FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_TOKEN_NO_VALID';
	ELSEIF 		= 0 THEN
		SELECT VAL_2 INTO MSG_EXPIRE FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
	END IF;
END$$
DELIMITER ;


