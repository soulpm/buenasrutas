	use db_buenas_rutas;
	-- ---------------------------------------------------------------------------------------------------------------
	-- VALIDATE TOKEN FUNCTION
	-- ---------------------------------------------------------------------------------------------------------------
	DROP FUNCTION IF EXISTS  FN_IS_USER_AUTHORIZED;
	DELIMITER $$
	CREATE FUNCTION FN_IS_USER_AUTHORIZED (p_token VARCHAR(255))
	RETURNS INT DETERMINISTIC
	BEGIN
	DECLARE NUM_VALID INT;
	DECLARE IS_AUTHORIZED INT; 
	DECLARE TIME_ELAPSED INT;
		SELECT COUNT(ID_USR) INTO NUM_VALID FROM cfg_acc_sys WHERE ACCESS_TOKEN = p_token;
		IF NUM_VALID >0 THEN
			SELECT (EXPIRES>NOW()) INTO TIME_ELAPSED FROM cfg_acc_sys
			WHERE ACCESS_TOKEN = p_token;
			IF TIME_ELAPSED = 1 THEN
				SET IS_AUTHORIZED = 1;
			ELSE 
				DELETE FROM cfg_acc_sys WHERE ACCESS_TOKEN = p_token;
				SET IS_AUTHORIZED = 0;
			END IF;
			SET IS_AUTHORIZED = TIME_ELAPSED;
		ELSE
			SET IS_AUTHORIZED = -1;	
		END IF;
	RETURN IS_AUTHORIZED;
	END$$
	DELIMITER ;

	
	-- b5d9ce14eb2e225ca0684cfe955a63ad9483efe1