use db_buenas_rutas;

drop table if exists VTA_TYP;

/*==============================================================*/
/* Table: VTA_TYP                                               */
/*==============================================================*/
create table VTA_TYP
(
   ID_VTA_TYP           int not null auto_increment comment 'IDENTIFICADOR TIPO DE VENTA',
   NAME                 varchar(85) comment 'NOMBRE TIPO DE VENTA',
   primary key (ID_VTA_TYP)
);

alter table VTA_TYP comment 'TIPO DE VENTA';


drop table if exists VTA_SALE;

/*==============================================================*/
/* Table: VTA_SALE                                              */
/*==============================================================*/
create table VTA_SALE
(
   ID_VTA               int not null auto_increment comment 'ID REGISTRO VENTA',
   ID_USR_CLI           int comment 'Identificador del Registro Cliente',
   ID_VTA_TYP           int comment 'IDENTIFICADOR TIPO DE VENTA',
   ADDRESS              varchar(255) comment 'DIRECCION CLIENTE',
   ID_USR_SAL_MAN       int comment 'Identificador del Registro Vendedor',
   ID_PRF               int comment 'IDENTIFICADOR DE PROFORMA',
   OBSERVATION          varchar(255) comment 'OBSERVACION DE VENTA',
   ID_PRD               int comment 'IDENTIFICADOR DE PRODUCTO',
   QUANTITY             int comment 'CANTIDAD DE PRODUCTOS',
   DISCOUNT             float comment 'DESCUENTO',
   TOTAL_DISCOUNT       float comment 'TOTAL DE DESCUENTO',
   PRICE                float comment 'PRECIO',
   IGV                  float comment 'IGV',
   SUBTOTAL             float comment 'SUBTOTAL DE VENTA',
   TOTAL                float comment 'TOTAL DE VENTA',
   primary key (ID_VTA)
);

alter table VTA_SALE comment 'Tabla Ventas';

alter table VTA_SALE add constraint FK_REFERENCIA_CLIENTE_VENTA foreign key (ID_USR_CLI)
      references SYS_USR (ID_USR) on delete restrict on update restrict;

alter table VTA_SALE add constraint FK_REFER_VENTA_TIPO foreign key (ID_VTA_TYP)
      references VTA_TYP (ID_VTA_TYP) on delete restrict on update restrict;

alter table VTA_SALE add constraint FK_REF_SALES_MAN foreign key (ID_USR_SAL_MAN)
      references SYS_USR (ID_USR) on delete restrict on update restrict;

alter table VTA_SALE add constraint FK_REF_SALE_PRODUCT foreign key (ID_PRD)
      references LOG_PROD (ID_PRD) on delete restrict on update restrict;

alter table VTA_SALE add constraint FK_REF_SALE_PROFORM foreign key (ID_PRF)
      references VTA_SALE (ID_VTA) on delete restrict on update restrict;
