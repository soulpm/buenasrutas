use db_buenas_rutas;
-- ---------------------------------------------------------------------------------------------------------------
-- INSERT ALMACEN
-- ---------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS SP_LOG_ALM_INS;
DELIMITER //
CREATE PROCEDURE SP_LOG_ALM_INS(
IN  P_TOKEN			VARCHAR(60)		,-- TOKEN USER
IN  P_CODE			VARCHAR(15)	 	,-- COD INTERNO 
IN  P_NAMES 		VARCHAR(80)	 	,-- NOMBRE ALMACEN
IN  P_ADDRESS		VARCHAR(60)		,-- DIRECCION
IN  P_AREA			INT				,-- AREA
OUT PMSG 			VARCHAR(255)	 -- MESSAGE
)
 BEGIN
	DECLARE P_IS_AUTH		INT;
	SELECT FN_IS_USER_AUTHORIZED(P_TOKEN) INTO P_IS_AUTH;
	IF P_IS_AUTH  = 1 THEN
		SELECT COUNT(*) INTO @NUM FROM log_almacen WHERE  NAME = P_NAMES;	
		IF @NUM =0 THEN
			INSERT INTO log_almacen(CODE,NAME,ADDRESS,AREA)
			VALUES (P_CODE,P_NAMES,P_ADDRESS,P_AREA);
			SET PMSG = 'Almacen Registrado';
	    ELSE 
			SET PMSG = 'Error, Ya existe un almacen con el nombre ingresado';
	    END IF;                    
	ELSEIF P_IS_AUTH = -1 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_TOKEN_NO_VALID';
	ELSEIF P_IS_AUTH	= 0 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
	END IF;
 END//
DELIMITER ;		
-- ---------------------------------------------------------------------------------------------------------------
-- UPDATE ALMACEN
-- ---------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS SP_LOG_ALM_UPD;
DELIMITER //
CREATE PROCEDURE SP_LOG_ALM_UPD(
IN  P_TOKEN			VARCHAR(60)		,-- TOKEN USER
IN  P_ID			INT				,-- CODIGO SISTEMA 
IN  P_CODE			VARCHAR(15)		,-- COD INTERNO 
IN  P_NAMES 		VARCHAR(80)	 	,-- NOMBRE ALMACEN
IN  P_ADDRESS		VARCHAR(60)		,-- DIRECCION
IN  P_AREA			INT				,-- AREA
OUT PMSG 			VARCHAR(255)	 -- MESSAGE
)
 BEGIN
	DECLARE P_IS_AUTH		INT;
	SELECT FN_IS_USER_AUTHORIZED(P_TOKEN) INTO P_IS_AUTH;
	IF P_IS_AUTH  = 1 THEN
		SELECT COUNT(*) INTO @NUM FROM log_almacen WHERE  NAME = P_NAMES AND ID_ALM <>P_ID;	
		IF @NUM =0 THEN
			UPDATE log_almacen SET 
			CODE 	= P_CODE,
			NAME	= P_NAMES,
			ADDRESS	= P_ADDRESS,
			AREA	= P_AREA 
			WHERE ID_ALM = P_ID; 
			SET PMSG = 'Almacen Modificado';
	    ELSE 
			SET PMSG = 'Error, Ya existe un almacen con el nombre ingresado';
	    END IF;                    
	ELSEIF P_IS_AUTH = -1 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_TOKEN_NO_VALID';
	ELSEIF P_IS_AUTH	= 0 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
	END IF;
 END//
DELIMITER ;		

-- ---------------------------------------------------------------------------------------------------------------
-- LIST WAREHOUSE
-- ---------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS  SP_LOG_ALM_LST;
DELIMITER $$
CREATE PROCEDURE SP_LOG_ALM_LST(
	IN P_TOKEN			VARCHAR(60),	-- TOKEN	
	IN P_ID				INT,			-- ID_ALM
	IN P_NAME 			VARCHAR(100), 	-- NAME
	OUT PMSG			VARCHAR(255)	-- MESSAGE 
)
BEGIN	
	DECLARE P_IS_AUTH INT;
	SELECT FN_IS_USER_AUTHORIZED(p_token) INTO P_IS_AUTH;
    IF P_IS_AUTH = 1 THEN
		SELECT 
			ID_ALM 	ID,
			CODE	CODE,
			NAME	NAME,
			ADDRESS	ADDRESS,
			AREA	AREA
		FROM log_almacen WHERE
		ID_ALM 	= CASE WHEN P_ID 	IS NULL OR P_ID 	= 0 THEN ID_ALM ELSE P_ID END AND 
		NAME LIKE CASE WHEN P_NAME 	IS NULL OR P_NAME 	= '' THEN NAME ELSE CONCAT('%',P_NAME,'%') END;
	ELSEIF P_IS_AUTH = -1 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_TOKEN_NO_VALID';
	ELSEIF P_IS_AUTH	= 0 THEN
		SELECT VAL_2 INTO PMSG FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
	END IF;
END$$
DELIMITER ;






