use db_buenas_rutas;

drop table if exists log_branch;

/*==============================================================*/
/* Table: log_branch                                            */
/*==============================================================*/
create table log_branch
(
   ID_BRANCH            int not null auto_increment,
   NAME                 varchar(85),
   primary key (ID_BRANCH)
);

alter table log_branch comment 'MARCA DE PRODUCTOS';

drop table if exists log_family_prod;

/*==============================================================*/
/* Table: log_family_prod                                       */
/*==============================================================*/
create table log_family_prod
(
   ID_FAM_PROD          int not null auto_increment comment 'IDENTIFICADOR DE REGISTRO FAMILIA PRODUCTOS',
   NAME                 varchar(85) comment 'NOMBRE DE FAMILIA',
   primary key (ID_FAM_PROD)
);

alter table log_family_prod comment 'FAMILIA DE PRODUCTOS';


drop table if exists log_almacen;

/*==============================================================*/
/* Table: log_almacen                                           */
/*==============================================================*/
create table log_almacen
(
   ID_ALM               int not null auto_increment comment 'IDENTIFICADOR DE REGISTRO DE ALMACEN',
   CODE                 varchar(15),
   NAME                 varchar(85) comment 'NOMBRE DE ALMACEN',
   ADDRESS              varchar(85),
   AREA                 varchar(25),
   primary key (ID_ALM)
);

alter table log_almacen comment 'ALMACEN DE PRODUCTOS';



drop table if exists log_prod;

/*==============================================================*/
/* Table: log_prod                                              */
/*==============================================================*/
create table log_prod
(
   ID_PRD               int not null comment 'IDENTIFICADOR DE REGISTRO PRDUCTO',
   CODE                 varchar(15),
   NAME                 varchar(150) comment 'NOMBRE DE PRODUCTO',
   BRANCH               int comment 'MARCAS DE PRODUCTOS',
   PROVEEDOR            int comment 'PROVEEDOR DE PRODUCTOS',
   ID_FAM_PROD          int comment 'IDENTIFICADOR DE REGISTRO FAMILIA PRODUCTOS',
   ID_ALM               int comment 'IDENTIFICADOR DE REGISTRO DE ALMACEN',
   LAST_BOUGHT_PRICE    float comment 'ULTIMO PRECIO DE COMPRA',
   PRICE_AVERAGE        float comment 'PRECIO PROMEDIO',
   PRICE                float comment 'PRECIO EFECTIVO',
   TARJETA_PRICE        float comment 'PRECIO POR TARJETA',
   STOCK                int comment 'STOCK DE PRODUCTOS',
   MIN_STOCK            int comment 'STOCK MINIMO',
   DAYS_STOCK           int comment 'DIAS DE STOCK',
   IMAGE                varchar(75) comment 'IMAGEN DE PRODUCTO',
   primary key (ID_PRD)
);

alter table log_prod comment 'TABLA DE PRODUCTOS';

alter table log_prod add constraint FK_REF_PRODUCT_ALMACEN foreign key (ID_ALM)
      references LOG_ALMACEN (ID_ALM) on delete restrict on update restrict;

alter table log_prod add constraint FK_REF_PRODUCT_BRANCH foreign key (BRANCH)
      references LOG_BRANCH (ID_BRANCH) on delete restrict on update restrict;

alter table log_prod add constraint FK_REF_PRODUCT_FAMILY foreign key (ID_FAM_PROD)
      references LOG_FAMILY_PROD (ID_FAM_PROD) on delete restrict on update restrict;

alter table log_prod add constraint FK_REF_PRODUCT_PROVEEDOR foreign key (PROVEEDOR)
      references SYS_USR (ID_USR) on delete restrict on update restrict;

	  
drop table if exists log_price;

/*==============================================================*/
/* Table: log_price                                             */
/*==============================================================*/
create table log_price
(
   ID_PRICE             int not null comment 'IDENTIFICADOR PRECIO',
   ID_PRD               int comment 'IDENTIFICADOR DE REGISTRO PRDUCTO',
   DATE_INI             date comment 'FECHA VALIDEZ INICIAL',
   DATE_END             date comment 'FECHA VALIDEZ FINAL',
   PRICE                float comment 'PRECIO',
   primary key (ID_PRICE)
);

alter table log_price comment 'LISTA PRECIOS POR PRODUCTO';

alter table log_price add constraint FK_REF_PRODUCT_LIST_PRICE foreign key (ID_PRD)
      references LOG_PROD (ID_PRD) on delete restrict on update restrict;
	  
	  