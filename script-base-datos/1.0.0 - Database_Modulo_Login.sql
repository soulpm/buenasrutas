drop database db_buenas_rutas;
create database db_buenas_rutas;
use db_buenas_rutas;
alter database db_buenas_rutas default character set utf8;
alter database db_buenas_rutas default collate utf8_unicode_ci;

drop table if exists cfg_par;

/*==============================================================*/
/* Table: CFG_PAR                                               */
/*==============================================================*/
create table cfg_par
(
   ID_PAR               int not null auto_increment comment 'Identificacion del Registro',
   VAL_1                varchar(25) comment 'Valor 1  del parametro',
   VAL_2                varchar(100) comment 'Valor 2  del parametro',
   VAL_3                varchar(100) comment 'Valor 3 del parametro',
   VAL_4                varchar(100) comment 'Valor 4 del parametro',
   DESCRIPTION          varchar(200) comment 'Descripcion del parametro',
   primary key (ID_PAR)
);

alter table cfg_par comment 'Parametros Generales del Sistema';

INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('STD','Activo','Estado Activo','','Estado de un registro del sistema');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('STD','Bloqueado','Estado Bloqueado','','Estado de un registro del sistema');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('TNIF','DNI','Documento Nacional de Identidad','','Tipo Documento');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('TNIF','RUC','Registro Unico del Contribuyente','','Tipo Documento');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('IMGU','user-image-default.jpg','','','Imagen Usuario default');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('SYS','Buenas Rutas Peru','COMPANY','','Nombre de la Compania');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('SYS','Sistema Integral Buenas Rutas Perú','NAME_SYS','','Nombre Sistema');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('SYS','00:10:00','TIME_EXPIRE','','Tiempo Expiracion Token Acceso');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('SYS','El tiempo de acceso al sistema ha expirado, inicie sesi&oacute;n nuevamente','MSG_EXPIRE','','Mensaje Expiracion Token Acceso');
INSERT INTO cfg_par (val_1,val_2,val_3,val_4,description)
VALUES ('SYS','Token No Valido','MSG_TOKEN_NO_VALID','','Mensaje Token Acceso No Valido');

-- SELECT VAL_2 FROM cfg_par WHERE VAL_1 = 'SYS' AND VAL_3 = 'MSG_EXPIRE';
drop table if exists CFG_MNU;

/*==============================================================*/
/* Table: CFG_MNU                                               */
/*==============================================================*/
create table cfg_mnu
(
   ID_MNU               int not null auto_increment comment 'Identificación del Registro',
   NAME                 varchar(100) comment 'Nombre del Menu',
   URL                  varchar(100) comment 'Direccion Url del Menu',
   DESCRIPTION          varchar(200) comment 'Detalle o descripcion del registro',
   ICON                 varchar(60)  comment 'Icono del Menu bajo Font Awesome 4',
   primary key (ID_MNU)
);

alter table cfg_mnu comment 'Menus del Sistema';
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Inicio','/init','Pagina Inicial del Sistema','fa fa-home');
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Administracion','/init/admin','Modulo de Administracion','fa fa-address-book');
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Logistica','/init/logistic','Modulo de Logistica: Productos, Almacen','fa fa-file-text-o');
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Compras','/init/purchase','Modulo de Compras','fa fa-file-text-o');
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Ventas','/init/sale','Modulo de Ventas','fa fa-money');
INSERT INTO cfg_mnu (NAME,URL,DESCRIPTION,ICON)
VALUES('Reportes','/init/reports','Modulo de Reportes','fa fa-bar-chart');


drop table if exists CFG_SUB_MNU;

/*==============================================================*/
/* Table: CFG_SUB_MNU                                               */
/*==============================================================*/
create table cfg_sub_mnu
(
   ID_MNU               int not null comment 'Identificación del Menu Padre',
   ID_SUB_MNU           int not null comment 'Identificación del Registro',
   NAME                 varchar(100) comment 'Nombre del Sub Menu',
   URL                  varchar(100) comment 'Direccion Url del Sub Menu',
   DESCRIPTION          varchar(200) comment 'Detalle o descripcion del registro',
   ICON                 varchar(60)  comment 'Icono del Sub Menu bajo Font Awesome 4',
   STATE				INT default 1 comment 'Estado del Sub Menu', 
   primary key (ID_MNU,ID_SUB_MNU)
);

alter table cfg_sub_mnu comment 'Sub Menus del Sistema';
-- ADMIN
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(2,1,'Usuarios','/init/users','Pagina Usuarios del Sistema','fa fa-address-book');
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(2,2,'Configuración','/init/config','Pagina Configuracion del Sistema','fa fa-home');
-- LOGISTIC
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(3,1,'Almacen','/logistic/warehouse','Pagina Almacenes','fa fa-home');
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(3,2,'Marcas','/logistic/brands','Pagina Marcas','fa fa-home');
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(3,3,'Familia Productos','/logistic/family-product','Pagina Familia de Productos','fa fa-home');
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(3,4,'Precios','/logistic/price','Pagina Precios','fa fa-home');
INSERT INTO cfg_sub_mnu (ID_MNU,ID_SUB_MNU,NAME,URL,DESCRIPTION,ICON)
VALUES(3,5,'Productos','/logistic/product','Pagina Productos','fa fa-home');



		

drop table if exists sys_rol;

/*==============================================================*/
/* Table: SYS_ROL                                               */
/*==============================================================*/
create table sys_rol
(
   ID_ROL               int not null auto_increment comment 'Identificacion del Registro',
   NAME                 varchar(55) comment 'Nombre del Registro',
   STATE                int comment 'Estado del Registro',
   primary key (ID_ROL)
);

alter table sys_rol comment 'Roles del Sistema';
INSERT INTO sys_rol (name,state) VALUES ('Administrador',1);
INSERT INTO sys_rol (name,state) VALUES ('Vendedor',1);
INSERT INTO sys_rol (name,state) VALUES ('Cajero',1);

/*==============================================================*/
/* Index: IDX_ROL_NAME                                          */
/*==============================================================*/
create index IDX_ROL_NAME on sys_rol
(
   NAME
);

alter table sys_rol add constraint FK_SYS_ROL_STATE foreign key (STATE)
      references CFG_PAR (ID_PAR) on delete restrict on update restrict;

		
drop table if exists sys_usr;

/*==============================================================*/
/* Table: SYS_USR                                               */
/*==============================================================*/
create table sys_usr
(
   ID_USR               int not null auto_increment comment 'Identificador del Registro',
   ID_ROL               int comment 'Identificador Rol Usuario',
   ID_USR_PARENT        int comment 'Usuario Padre Relacionado',
   NAMES                varchar(100) comment 'Nombres del Usuario',
   EMAIL                varchar(60) comment 'Correo Electronico',
   TYP_NIF              int comment 'Tipo Documento',
   NIF                  varchar(15) comment 'Numero Documento',
   IDENTIFICATION       varchar(60) comment 'Identificacion Sistema',
   PASSWORD             varchar(60) comment 'Clave del Sistema',
   SEX					char(1)	comment 'Genero Sexual del Usuario',
   ADDRESS				VARCHAR(250) comment 'Direccion Fisica del Usuario' DEFAULT '',
   MOVILNUMBER			VARCHAR(15)	comment 'Numero Movil del Usuario' DEFAULT '',
   LANDLINE				VARCHAR(15)	comment 'Numero Fijo del Usuario' DEFAULT '',
   PHOTO                varchar(45) comment 'Foto',
   STATE                int comment 'Estado Usuario',
   primary key (ID_USR)
);

alter table sys_usr comment 'Usuarios del Sistema';

INSERT INTO sys_usr(id_rol,names,email,photo,typ_nif,nif,identification,password,state,sex,movilnumber)
VALUES (1,'Saúl José Pérez Mozombite','sauljose1301@gmail.com','user-image-default.jpg',3,'45108574',sha1(sha1('saul')),sha1(sha1('saul')),1,'M','993615593');

INSERT INTO sys_usr(id_rol,names,email,photo,typ_nif,nif,identification,password,state,sex,movilnumber)
VALUES (1,'Walter Torres','waltertorres1984@gmail.com','user-image-default.jpg',3,'',sha1(sha1('walter')),sha1(sha1('walter')),1,'M','935170927');

INSERT INTO sys_usr(id_rol,names,email,photo,typ_nif,nif,identification,password,state,sex,movilnumber)
VALUES (1,'Michel','','user-image-default.jpg',3,'',sha1(sha1('michel')),sha1(sha1('michel')),1,'M','');

alter table sys_usr add constraint FK_SYS_STATE foreign key (STATE)
      references CFG_PAR (ID_PAR) on delete restrict on update restrict;

alter table sys_usr add constraint FK_SYS_USR_ROL foreign key (ID_ROL)
      references SYS_ROL (ID_ROL) on delete restrict on update restrict;
 
drop table if exists cfg_acc_sys;

/*==============================================================*/
/* Table: CFG_ACC_SYS                                           */
/*==============================================================*/
create table cfg_acc_sys
(
   ACCESS_TOKEN         VARCHAR(255) not null comment 'Valor Token de Acceso',
   ID_USR               int not null comment 'Identificador de Usuario ',
   EXPIRES              timestamp comment 'Tiempo Expiracion del Token',
   SCOPE                VARCHAR(250) comment 'Ambito del Token',
   primary key (ID_USR)
);

alter table cfg_acc_sys comment 'Autorizacion de Acceso al Sistema';

alter table cfg_acc_sys add constraint FK_SYS_ACCESS foreign key (ID_USR)
      references sys_usr (ID_USR) on delete restrict on update restrict;
	  

drop table if exists sys_mnu_rol;

/*==============================================================*/
/* Table: SYS_MNU_ROL                                           */
/*==============================================================*/
create table sys_mnu_rol
(
   ID_MNU_ROL           int not null auto_increment comment 'Identificacion del Registro',
   ID_MNU               int comment 'Identificador del Menu del Sistema',
   ID_ROL               int comment 'Identificador del Rol del Sistema',
   primary key (ID_MNU_ROL)
);

alter table sys_mnu_rol comment 'Menus por Roles del Sistema';

-- MENU ADMINISTRADOR
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (1,1);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (2,1);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (3,1);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (4,1);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (5,1);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (6,1);

-- MENU VENDEDOR
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (1,2);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (5,2);
INSERT INTO sys_mnu_rol (id_mnu,id_rol)
VALUES (6,2);



alter table sys_mnu_rol add constraint FK_SYS_MNU_ID_MNU foreign key (ID_MNU)
      references CFG_MNU (ID_MNU) on delete restrict on update restrict;

alter table sys_mnu_rol add constraint FK_SYS_MNU_ID_ROL foreign key (ID_ROL)
      references SYS_ROL (ID_ROL) on delete restrict on update restrict;


