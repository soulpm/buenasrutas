import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { InitComponent } from './component/pages/init/init.component';
import { PageNotFoundComponent } from './component/shared/page-not-found/page-not-found.component';


const routerPaths:Route[] = [
  { path : ''       ,component:LoginComponent           },
  { path : 'login'  ,component:LoginComponent           },
  { path : '**'     , component: PageNotFoundComponent  }
];


@NgModule({
  imports: [RouterModule.forRoot(routerPaths)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}


