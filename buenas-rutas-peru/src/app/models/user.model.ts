export interface IUserModel {
    userId?:number;
    role?:IRoleModel;
    typeNif?:IDocumentModel;
    numberNif?:string;
    names?:string;
    email?:string;
    idSystem?:string;
    password?:string;
    image?:string;
    sex?:string;
    address?:string;
    movilNumber?:string;
    landLine?:string;
    estate?:IStateModel;
    tokenSession?:string;
    asignado?:boolean;
}
export interface IDocumentModel {
    idDocument?:number;
    name?:string;
}
export interface IStateModel {
    idState?:number;
    name?:string;
}
export interface ILoginResponse {
    userId?: string;
    password?: string;
}

export interface IUserAccount {
    userId?:number;
    role?:IRoleModel;
    names?:string;
    email?:string;
    typeNif?:string;
    nif?:string;
    photo?:string;
    state?:string;
    tokenSession?:string;
}
export interface IRoleModel{
    idRole?:number
    name?:string;
    state?:string;
}
export interface ILoginResponse{
    stateLogin?:number;
    isLoginCorrect?:boolean;
    isFailure?:boolean;
    showMessage?:boolean;
    typeMessage?:string;
    titleMessage?:string;
    message?:string;
    userAccount?:IUserAccount;     
}
export interface ISexModel{
    idRegister?:string;
    name?:string; 
}

export interface IProveedorModel{
    id?:string;
}

export interface IUserFilterModel{
    token_session?:string;
    id?:number;
    name?:string;
    role?:number;
    typeDocument?:number;
    state?:number;
}