export interface IProductoModel{
    codigo?:number;
    producto?:string;
    cantidad?:number;
    precio?:number;
    descuento?:number;
    total?:number;
    totalgeneral?:number;
    
}

export interface IComprasModel{
    codigo?:number;
    producto?:string;
    cantidad?:number;
    precio?:number;
    total?:number;
}


