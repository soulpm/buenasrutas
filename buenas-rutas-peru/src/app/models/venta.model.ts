import { interceptingHandler } from "@angular/common/http/src/module";

export interface IProductoModel{
    codigo?:number;
    producto?:string;
    cantidad?:number;
    precio?:number;
    descuento?:number;
    total?:number;
    totalgeneral?:number;
    
}

export interface ITipopagoModel{
    id?:number;
    pago?:string;
}

export interface IBusqudaProdModel{
    codigo?:number;
    producto?:string;
    marca?:string;
    precio?:number;
    stock?:number;
}

export interface IMonedaModel{
    id?:number;
    moneda?:string;
}

export interface IVendedorModel{
    id?:number;
    vendedor?:string;
}



