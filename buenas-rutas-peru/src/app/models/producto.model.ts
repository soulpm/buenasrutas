import { IProveedorModel } from "./user.model";

export interface IAlmacenModel{
    idAlmacen       ?: number;
    Almacen         ?: string;
    
}

export interface IBranchModel{

}

export interface IProductoModel{
    idProduct?:number;
    code?:string;
    name?:string;
    branch?:IMarca;
    proveedor;
    familia;
    almacen;
    ultimoPrecioCompra;
    precioPromedio;
    precio;
    precioTarjeta;
    stock;
    stockMinimo;
    stockDias;
    image;

}

export interface IFilterProductModel{
    almacen?:IAlmacenModel;
    proveedor?:IProveedorModel;

}

export interface IMarca {

}
export interface IAgregaProdModel{
    codigo?:number;
    producto?:string;
    marca?:string;
    familia?:string;
    proveedor?:string;
    preciocompra?:number;
    precioventa?:number;
    imagen?:string;


}


