import { HttpClientModule}    from '@angular/common/http';
import { NgModule } from '@angular/core';
import { InitComponent } from './pages/init/init.component';
import { SharedModule } from './shared/shared.module';
import { PAGES_ROUTE } from './pages.route';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UsuarioComponent } from './pages/admin/user.component';
import { VentasComponent } from './pages/ventas/ventas.component';
import { LogisticComponent } from './pages/logistic/logistic.component';
import { ComprasComponent } from './pages/compras/compras.component';
import { ModalBuscarComponent } from './pages/modal/ventas/modal-buscar/modal-buscar.component';
import { ModalDocumentoComponent } from './pages/modal/ventas/modal-documento/modal-documento.component';
import { CajasComponent } from './pages/cajas/cajas.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { ModalAgregarComponent } from './pages/modal/reportes/modal-agregar/modal-agregar.component';
import { ModalAgregarComprasComponent } from './pages/modal/reportes/modal-agregar-compras/modal-agregar-compras.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ModalUserComponent } from './pages/modal/user/modal-user/modal-user.component';
import { ModalCredentialUserComponent } from './pages/modal/user/modal-credential-user/modal-credential-user.component';
import { WarehouseComponent } from './pages/logistic/warehouse/warehouse.component';


@NgModule({
    declarations: [
        InitComponent,
        UsuarioComponent,
        VentasComponent,
        LogisticComponent,
        ComprasComponent,
        ModalBuscarComponent,
        ModalDocumentoComponent,
        CajasComponent,
        ReportesComponent,
        ModalAgregarComponent,
        ModalAgregarComprasComponent,
        ModalUserComponent,
        ModalCredentialUserComponent,
        WarehouseComponent
    ],
    exports: [
        
    ],
    imports: [
        SharedModule,
        PAGES_ROUTE,
        FormsModule,
        CommonModule,
        NgbModule,
        TableModule,
        DropdownModule,
        BrowserAnimationsModule
    ]
})
export class PagesModule {}
