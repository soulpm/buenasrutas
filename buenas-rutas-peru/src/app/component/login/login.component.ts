import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { ILoginResponse } from 'src/app/models/user.model';
import * as sha1 from '../../../../node_modules/js-sha1/src/sha1.js';
import * as $ from 'jquery';
declare var $:$;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  inputPassword:string            = "";
  inputUserName:string                = "";
  pathImage:string;
  loginResponse:ILoginResponse    = {};
  loginInfo:ILoginResponse        = {};
  isLoading:boolean;

  constructor(private router:Router,private userService:UserService) { }

  ngOnInit() {
    let obj = this;
    
  }

  loginUser():void{
    let obj = this;
    if(!obj.isLoading){
      
      obj.validateData();
      if(obj.loginResponse.isLoginCorrect){
          obj.loginResponse = {};
          obj.loginInfo.userId    = sha1(obj.inputUserName);
          obj.loginInfo.password  = sha1(obj.inputPassword);
          obj.isLoading = true;
          obj.userService.loginAccount(obj.loginInfo).then(
            (val) => {
              obj.isLoading = false;
              obj.loginResponse = val;
              setTimeout(function(){obj.router.navigate([environment.initPage]);},1000);
            },
            (err) => {
              $("#inputUserName").focus();
              $("#inputUserName").select();
              obj.isLoading =false;
              err.titleMessage = "Error Inicio Sesión!";
              if(err.message == null){err.message = "El servicio de inicio de sesión no está funcionando, consulte al administrador del sistema";}
              obj.loginResponse = err
            });
      }
    }
  }
  validateData():void{
    let obj = this;
    let messageTemp:string                   = "";
    obj.loginResponse.isLoginCorrect        = false;
    if(obj.inputUserName==""){
      messageTemp = " Usuario"; 
    }
    if(obj.inputPassword==""){
     if(obj.inputPassword!=""){messageTemp += " Contraseña";}
     else{messageTemp += " y Contraseña";}
    }
    if(messageTemp!=""){
       messageTemp = "Ingrese los campos: "+messageTemp;
       $("#inputUserName").focus();
       obj.loginResponse.typeMessage  = "alert-warning";
       obj.loginResponse.titleMessage = "Error Validación Datos!"; 
       obj.loginResponse.message      = messageTemp;
       obj.loginResponse.showMessage  = true;
    }
    else{ obj.loginResponse.isLoginCorrect        = true;}
 }

}