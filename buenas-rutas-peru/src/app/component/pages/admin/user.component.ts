import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { IRoleModel, IDocumentModel, IStateModel, IUserModel, IUserFilterModel, ISexModel } from 'src/app/models/user.model';
import { IColumnTable, IDropDownModel, IResultResponse } from 'src/app/models/config.model';
import { ColumnTables } from 'src/app/utilitary/column_tables';
import { ConfigService } from 'src/app/services/config.service';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';
declare var $:$;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UsuarioComponent implements OnInit {

  filterIdUser:number                     = 0;
  filterName:string                       = "";
  filterRole:IDropDownModel               = { value:"0"};
  filterTypeDoc:IDropDownModel            = { value:"0"};
  filterState:IDropDownModel              = { value:"0"};
  inputId:string;
  inputConfirmId:string;
  inputPassword:string;
  inputConfirmPassword:string;
  listRoles:IDropDownModel[]                  = [];
  listTypeDocument:IDropDownModel[]       = [];
  listState:IDropDownModel[]                 = [];
  modalRegisterName:string                = "modalUser";
  modalCredentialName:string              = "modalUserCredential";     
  listUsers:IUserModel[]                  = [];
  userListLength:number                   = 0;
  listUsersColumns:IColumnTable[]         = [];
  listClients:IUserModel[]                = [];
  listSexInput:ISexModel[]  = [];    
  isEditMode:boolean                      = false;
  userEditParent:IUserModel               = {role:{},typeNif:{},estate:{}};
  titleModal:string;  
  parentComponent:UsuarioComponent;
  userLength:number = 0;
  tokenUserSession:string = "";
  root_path_image:string  = environment.pathImage;
  constructor(
    private userService     : UserService,  
    private configService   : ConfigService,
    private columnsUtility  : ColumnTables
  ) { }

  ngOnInit() {
    let obj = this;
    obj.tokenUserSession  = sessionStorage.getItem("user_session");
    obj.listUsersColumns  = obj.columnsUtility.getUserColumns();  
    obj.parentComponent   = this;
    obj.loadFilters();
  }

  getListUser():void{
      let obj = this;
      obj.loadUsers();
  }

  loadFilters():void{ 
    let obj = this;

    obj.listSexInput = [
        {idRegister:"M",name:"Masculino"},
        {idRegister:"F",name:"Femenino"}
    ];
    obj.userService.getListFilterUser(obj.tokenUserSession).then(
      (val) => {
        let objResponse:IResultResponse = val;
          switch(objResponse.responseCode){
                case 200:
                let listFilter:IDropDownModel[] = objResponse.data;
                let allKind:IDropDownModel = {value:"0",label:"--Todos--"};
                obj.listRoles.push(allKind);
                obj.listTypeDocument.push(allKind);
                obj.listState.push(allKind);
                for(let j=0;j<listFilter.length;j++){
                  switch(listFilter[j].type){
                      case "1": obj.listRoles.push(listFilter[j]);        break;
                      case "2": obj.listTypeDocument.push(listFilter[j]); break;
                      case "3": obj.listState.push(listFilter[j]);        break;
                  } 
                }
                obj.filterState   = obj.listState[0];
                obj.filterTypeDoc = obj.listTypeDocument[0];
                obj.filterRole    = obj.listRoles[0];
                obj.loadUsers();
            break;
            case -900:
                obj.configService.showModalExpiration();  
            break;
          } 
        
      } , 
      (err) => {
          obj.configService.showAlertMessage("Listar Filtros","Ocurrio un error inesperado","alert-danger");
      });
    }
    loadUsers():void{ 
      let obj = this;
      obj.listUsers       = null;
      obj.userListLength  = 0;
      let roleSelect    : any = (obj.filterRole.value!=null)?obj.filterRole.value:obj.filterRole;
      let typeDocSelect : any = (obj.filterTypeDoc.value!=null)?obj.filterTypeDoc.value:obj.filterTypeDoc;
      let stateSelect   : any = (obj.filterState.value!=null)?obj.filterState.value:obj.filterState;
      
      let filter:IUserFilterModel = {
          token_session : obj.tokenUserSession,
          id            : 0,
          name          : obj.filterName,
          role          : parseInt(roleSelect),
          typeDocument  : parseInt(typeDocSelect),
          state         : parseInt(stateSelect)
      }
      obj.userService.getListUser(filter).then(
        (val) => {
          let objResponse:IResultResponse = val;
            switch(objResponse.responseCode){
              case 200:
                   obj.listUsers      =  objResponse.data;
                   obj.userListLength = obj.listUsers.length;
              break;
              case -900:
                obj.configService.showModalExpiration();
              break;
            } 
        }, 
        (err) => {
              obj.configService.showAlertMessage("Listar Usuarios","Ocurrio un error inesperado","alert-danger");
        });
    }

    showNewUserModal():void{
      let obj = this;
      obj.isEditMode = false;
      obj.titleModal = "Registro Usuario";
      obj.userEditParent = {
        role:{idRole:1},typeNif:{idDocument:3},estate:{idState:3},
        sex:obj.listSexInput[0].idRegister,
        movilNumber:"",address:"",landLine:"",email:""};
      $("#"+obj.modalRegisterName).modal('show');
      $("#"+obj.modalRegisterName).on('shown.bs.modal', function() {
        $("#inputName").focus();
      });
    }
    showEditUserModal(userEdit:IUserModel):void{
      let obj = this;
      obj.isEditMode = true;
      obj.titleModal = "Editar Usuario";
      obj.userEditParent = userEdit;  
      $("#"+obj.modalRegisterName).modal('show');    
      $("#"+obj.modalRegisterName).on('shown.bs.modal', function() {
        $("#inputName").select();
      });
    }
    showModalCredential(userEdit:IUserModel):void{
      let obj = this;
      obj.isEditMode = false;
      obj.titleModal = "Credenciales de Usuario";
      obj.userEditParent = userEdit;
      obj.inputPassword = "";
      obj.inputConfirmPassword = "";
      obj.inputId = "";
      obj.inputConfirmId = "";
      $("#"+obj.modalCredentialName).modal('show'); 
      $("#"+obj.modalCredentialName).on('shown.bs.modal',function(e){
         $("#inputId").focus();
      });
    }
    

}
