import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config.service';
import { ISubMenu, IMenu, IResultResponse } from 'src/app/models/config.model';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {

  titlePage       : string;
  parentComponent : InitComponent;
  menuSelect      : IMenu;
  subMenuList     : ISubMenu[]  = [];
  constructor(
    private route         : ActivatedRoute,
    private router        : Router,
    private configService : ConfigService)
     { }

  ngOnInit() {
    let obj = this;
    obj.parentComponent = this;
    obj.route
      .queryParams
      .subscribe(params => {
        obj.titlePage   = params['p'] || '';
      });  
    
  }

  

  
}
