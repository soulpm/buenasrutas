import { Component, OnInit } from '@angular/core';
import {IAlmacenModel, IFilterProductModel} from 'src/app/models/producto.model';
import {IProductoModel} from 'src/app/models/producto.model';
import {IAgregaProdModel} from 'src/app/models/producto.model';
declare var $:$;
import * as $ from 'jquery';
import { LogisticService } from 'src/app/services/logistic.service';

@Component({
  selector: 'app-logistic',
  templateUrl: './logistic.component.html',
  styleUrls: ['./logistic.component.css']
})
export class LogisticComponent implements OnInit {

  producto                : string;
  marca                   : string;
  familia                 : string;
  almacen                 : string;
  proveedor               : string;
  listAlmacen             : IAlmacenModel[];
  listProductoP           : IProductoModel[];
  parentComponent         : LogisticComponent;
  modalBuscarProductoName : string = "modal-agregar-producto";
  listAgregaProducto      : IAgregaProdModel[];
  titleDocument           : string;

  constructor(
    private productService:LogisticService) { }

  ngOnInit() {
    let obj = this;
    /*
    obj.cargarAlmacen();
    obj.cargaProductoP();
    obj.cargaAgregaProducto();
    */

    obj.parentComponent = this;
  }


  cargarAlmacen():void{
    let obj = this;
      obj.listAlmacen=[
      {idAlmacen:1, Almacen:"alm 1"},
      
     ];
   }

   cargaProductoP():void{
   let obj = this;
   /*obj.listProductoP=[
   {codigo:123, producto:"producto1", marca:"marca1", familia:"familia 1", proveedor:"proveedor 1",
    preciocompra: 123.00, precioventa: 124.50,imagen:"img"}
    
   ]*/
   let filter:IFilterProductModel = {}
   obj.productService.listProduct(filter).then(
        response=>{
          if(response.length>0)
          obj.listProductoP = response;
          //else
          //obj.utilClass.showMessage("No se encontraron resultados","alert-info");
        },
        error=>{
          //obj.utilClass.showMessage("Ocurrio un error, \n"+JSON.stringify(error),"alert-danger");
        } 
    );
    }
    cargaAgregaProducto():void{
        let obj = this;
        obj.listAgregaProducto=[
        {codigo:123, producto:"producto1", marca:"marca1", familia:"familia 1", proveedor:"proveedor 1",
        preciocompra: 123.00, precioventa: 124.50, imagen:"img"}
        ]
    }

    showModalAgregarProducto():void{
      let obj = this;
      obj.titleDocument = "Agregar Productos";
      $("#"+obj.modalBuscarProductoName).modal('show');
      
    }

}