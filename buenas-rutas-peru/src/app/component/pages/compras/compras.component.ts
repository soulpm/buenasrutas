import { Component, OnInit } from '@angular/core';
declare var $:$;
import * as $ from 'jquery';
import { IComprasModel } from 'src/app/models/compras.model';


@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {
  
  ruc: number;
  proveedor: string;
  tipopago: string;
  local: string;
  diascredito: string;
  fechaoc: Date;
  fechallegada: Date;
  oc: number;
  buscar: string;
  modalBuscarComprasName:string = "modal-buscar-compras";
  titleDocument:string;
  parentComponent:ComprasComponent;

  listCompras: IComprasModel[];

  constructor() { }

  ngOnInit() {
    let obj = this;
    obj.cargaAgregaCompras();
    obj.parentComponent = this;

  }

  cargaAgregaCompras():void{
    let obj = this;
    obj.listCompras=[
    {codigo:123, producto:"producto1", cantidad:2, precio:120.00,total:240.00}
 
    ]
      }


  showModalBuscarCompras():void{
    let obj = this;
    obj.titleDocument = "Buscar Productos";
    $("#"+obj.modalBuscarComprasName).modal('show');
    
  }



}
