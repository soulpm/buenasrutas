import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { ConfigService } from 'src/app/services/config.service';
import { UserService } from 'src/app/services/user.service';
declare var $:$;

@Component({
  selector: 'app-modal-credential-user',
  templateUrl: './modal-credential-user.component.html',
  styleUrls: ['./modal-credential-user.component.css']
})
export class ModalCredentialUserComponent implements OnInit {

  @Input('parentObj') parent:any;

  constructor(
    private configService : ConfigService,
    private userService   : UserService
    ) { }

  ngOnInit() {
  }

  saveCredentials():void{
      let obj = this;
      if(obj.validateFields()){
          obj.userService.changeCredential(
            obj.parent.tokenUserSession,
            obj.parent.userEditParent.userId,
            obj.parent.inputId,
            obj.parent.inputPassword
          ).then(
                (val) => {
                      let message:string =  val;
                      if(message.indexOf("Error")!=-1)
                      {obj.configService.showAlertMessage("Modificación Credencial Usuario",message,"alert-danger");}
                      else{ 
                        obj.configService.showAlertMessage("Modificación Credencial Usuario",message,"alert-success");
                        setTimeout(function(){
                          $("#"+obj.parent.modalCredentialName).modal('hide');
                        },2000);
                      }
                } ,
                (err) => {
                    let message =  "Ocurrio un error, "+err;
                    obj.configService.showAlertMessage("Modificación Credencial Usuario",message,"alert-danger");
                });
      }
  }
  validateFields():boolean{
      let obj = this;
      let valueReturn:boolean = false;
      if(obj.parent.inputId !="" || obj.parent.inputPassword !=""){

            if(obj.parent.inputId!=""){
                valueReturn = false;
                if(obj.parent.inputConfirmId != "" ){
                    if(obj.parent.inputId != "" &&  obj.parent.inputConfirmId !=""){          
                      if(obj.parent.inputId == obj.parent.inputConfirmId){
                          valueReturn = true;
                      }
                      else{obj.configService.showAlertMessage("Modificación Credencial",
                      "Los campos de identificación no coinciden, verifique","alert-warning");}
                    }
                    else{obj.configService.showAlertMessage("Modificación Credencial",
                    "Ingrese identificación para  modificar","alert-warning");}
                }
                else{ obj.configService.showAlertMessage("Modificación Credencial",
                "Debe ingresar la confirmación de identificación para modificar","alert-warning");}
              }

            if(obj.parent.inputPassword != "" ){
              valueReturn = false;
              if(obj.parent.inputPassword != "" ){
                  if(obj.parent.inputPassword != "" &&  obj.parent.inputConfirmPassword !=""){          
                    if(obj.parent.inputPassword == obj.parent.inputConfirmPassword){
                        valueReturn = true;
                    }
                    else{obj.configService.showAlertMessage("Modificación Credencial",
                    "Las contraseñas no coinciden, verifique","alert-warning");}
                  }
                  else{obj.configService.showAlertMessage("Modificación Credencial",
                  "Ingrese confirmación de contraseña a modificar","alert-warning");}
              }
              else{ obj.configService.showAlertMessage("Modificación Credencial",
              "Debe ingresar una contraseña para modificar","alert-warning");}
            }
      }
      else{obj.configService.showAlertMessage("Modificación Credencial",
      "Debe ingresar una identificación o contraseña o ambas para modificar","alert-warning");}
      return valueReturn;   
  }


}
