import { Component, OnInit, Input } from '@angular/core';
import { IProductoModel } from 'src/app/models/producto.model';

@Component({
  selector: 'app-modal-agregar',
  templateUrl: './modal-agregar.component.html',
  styleUrls: ['./modal-agregar.component.css']
})
export class ModalAgregarComponent implements OnInit {


  nombre:string;
  precioventa: number;
  preciocompra: number;
  ruc:number;
  marca: string;
  proveedor: string;
  imagen: string;
  familia: string;
  codigo: string;
  producto: string;

  productoAgrega:IProductoModel;

  constructor() { }

  ngOnInit() {
  }
  selectProducto():void{
    let obj = this;
    /*obj.productoAgrega = {
      idProduct:4,name:"prod tes",branch:{" marca test",familia:"familia test",proveedor:"prove test",preciocompra:33.00,precioventa: 35.00,imagen:"img test"
    };*/
    //obj.parent.listProductoP.push(obj.productoAgrega);
  }
}

