import { Component, OnInit, Input } from '@angular/core';
import { IComprasModel } from 'src/app/models/compras.model';
import { ComprasComponent } from '../../../compras/compras.component';

@Component({
  selector: 'app-modal-agregar-compras',
  templateUrl: './modal-agregar-compras.component.html',
  styleUrls: ['./modal-agregar-compras.component.css']
})
export class ModalAgregarComprasComponent implements OnInit {


  @Input("parentObj") parent:ComprasComponent;
  ComprasSelect:IComprasModel;

  constructor() { }

  ngOnInit() {
  }

  selectCompras():void{
    let obj = this;
    obj.ComprasSelect = {
      codigo:4,producto:"prod test",cantidad:2,precio:130,total:260
    };
    obj.parent.listCompras.push(obj.ComprasSelect);

}
  

}
