import { Component, OnInit, Input } from '@angular/core';
import { VentasComponent } from '../../../ventas/ventas.component';
import { IProductoModel } from 'src/app/models/venta.model';

@Component({
  selector: 'app-modal-buscar',
  templateUrl: './modal-buscar.component.html',
  styleUrls: ['./modal-buscar.component.css']
})
export class ModalBuscarComponent implements OnInit {
  buscar: string;


  @Input("parentObj") parent:VentasComponent;
  productoSelect:IProductoModel;

  constructor() { }

  ngOnInit() {
  }

  selectProducto():void{
      let obj = this;
      obj.productoSelect = {
        codigo:4,cantidad:15,descuento:0,precio:3.30,producto:"Test casco",total:3.30
      };
      obj.parent.listProducto.push(obj.productoSelect);

  }
}
