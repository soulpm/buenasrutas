import { Component, OnInit, Input } from '@angular/core';
import { VentasComponent } from '../../../ventas/ventas.component';

@Component({
  selector: 'app-modal-documento',
  templateUrl: './modal-documento.component.html',
  styleUrls: ['./modal-documento.component.css']
})
export class ModalDocumentoComponent implements OnInit {

  vuelto: number;
  total: number;
  tipopago: number;
  efectivo: number;


  @Input("parentObj") parent:VentasComponent;
  
  constructor() { }

  ngOnInit() {

  }
  

}
