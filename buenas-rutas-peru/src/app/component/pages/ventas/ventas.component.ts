import { Component, OnInit, Input } from '@angular/core';
import { IProductoModel, IVendedorModel} from 'src/app/models/venta.model';
import {ITipopagoModel} from 'src/app/models/venta.model';
import {IBusqudaProdModel} from 'src/app/models/venta.model';
import {IMonedaModel} from 'src/app/models/venta.model';
import * as $ from 'jquery';
import { from } from 'rxjs';
import { NgForOf } from '@angular/common';
declare var $:$;


@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  nombre:string;
  tipopago:string;
  ruc:number;
  direccion:string;
  observaciones: string;
  proforma: string;
  buscar: string;
  precio: number;
  cantidad: number;
  totalgeneral: number;
  precioventa: number;
  preciocompra: number;
  codigo: number;
  imagen: string;
  proveedor: string;
  vendedor: string;
  parentComponent:VentasComponent;
  vuelto: number;
  total: number;
  tipomoneda: string;
  titleDocument:string;
  nameDocument:string = "modal-document-venta";
  modalBuscarProductoName:string = "modal-buscar-producto";
  listProducto:IProductoModel[];
  listTipoPago:ITipopagoModel[];
  listBusquedaProducto:IBusqudaProdModel[];
  listMoneda:IMonedaModel[];
  listVendedor:IVendedorModel[];
  
  @Input("parentObj") parent:VentasComponent;
  
  constructor() {

   }

  ngOnInit() {
      let obj = this;
      obj.cargarProductos();
      obj.cargarTipoPago();
      obj.cargarBusquedaProductos();
      obj.cargarTipoMoneda();
      obj.cargarVendedor();
      
      obj.parentComponent = this;
      
  }

  cargarProductos():void{
      let obj = this;
      
      obj.listProducto = [
          {codigo:1,producto:"Producto 1",precio:14.56,cantidad:1,descuento:0,total:0},
          {codigo:2,producto:"Producto 2",precio:14.56,cantidad:3,descuento:0,total:0},
          {codigo:3,producto:"Producto 3",precio:14.56,cantidad:2,descuento:0,total:0}
          
      ];
  }

    cargarBusquedaProductos():void{
        let obj = this;
        obj.listBusquedaProducto = [
            {codigo:1,producto:"producto 1", marca:"marca 1",precio:12.50, stock:21}
    ];

}   

  showModalDocument(tipo:string):void{
    let obj =this;
    switch(tipo){
        case "factura":
            obj.titleDocument = "Factura de Venta";
        break;
        case "boleta":
            obj.titleDocument = "Boleta Venta"
        break;
        case "ticket":
            obj.titleDocument = "Ticket Venta"
        break;
        case "proforma":
            obj.titleDocument = "Proforma Venta"
        break;
        case "remito":
            obj.titleDocument = "Remito Venta"
        break;
        case "notacredito":
            obj.titleDocument = "Nota Crédito Venta"
        break;
    }
    $("#"+obj.nameDocument).modal('show');
  }

  showModalBuscarProducto():void{
    let obj = this;
    obj.titleDocument = "Buscar Productos";
    $("#"+obj.modalBuscarProductoName).modal('show');
    
  }

  cargarTipoPago():void{
   let obj = this;
     obj.listTipoPago=[
     {id:1, pago:"Efectivo"},
     {id:2, pago:"Credito"},
     {id:3, pago:"Tarjeta"},
     {id:4, pago:"Transferencia"}
    ];
  }

  cargarTipoMoneda():void{
      let obj = this;
      obj.listMoneda=[
          {id:1, moneda:"Soles"},
          {id:2, moneda:"Dolares"}
      ];
  }

  cargarVendedor():void{
    let obj = this;
      obj.listVendedor=[
      {id:1234, vendedor:"Juan Peres"},
      {id:2345, vendedor:"Michel"},
      
     ];
   }
}
