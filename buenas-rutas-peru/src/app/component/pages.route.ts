import { Routes, RouterModule } from '@angular/router';
import { Component } from '@angular/core';
import { ComprasComponent } from './pages/compras/compras.component';
import { LogisticComponent } from './pages/logistic/logistic.component';
import { UsuarioComponent } from './pages/admin/user.component';
import { VentasComponent } from './pages/ventas/ventas.component';
import { InitComponent } from './pages/init/init.component';
import { CajasComponent } from './pages/cajas/cajas.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { WarehouseComponent } from './pages/logistic/warehouse/warehouse.component';



const pagesRoutes: Routes = [
    {   path: 'init',
        component: InitComponent,
        children: [
            { path  : 'purchases'   ,  component: ComprasComponent   },
            { path  : 'logistic'    ,  component: LogisticComponent  },
            { path  : 'admin'       , component:UsuarioComponent     },
            { path  : 'sales'       , component:VentasComponent      },
            { path  : 'boxes'       , component:CajasComponent       },
            { path  : 'reports'     , component:ReportesComponent    }
        ]
    },
    {
        path: 'logistic',
        component:InitComponent,
        children:[
            {path:'warehouse', component:WarehouseComponent }
        ]
    }
    
];
export const PAGES_ROUTE = RouterModule.forChild( pagesRoutes );
