import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { $ } from 'protractor';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-expiration-module',
  templateUrl: './expiration-module.component.html',
  styleUrls: ['./expiration-module.component.css']
})
export class ExpirationModuleComponent implements OnInit {

  private titleModal:string         = "Sesión del Sistema";
  private messageExpiration:string  = "Su sesión en el sistema ha expirado, por favor vuelva a Iniciar Sesión.";
  constructor(
    private userService   : UserService,
    private configService : ConfigService) { }

  ngOnInit() {

  }
  logout():void{
    let obj = this;
    obj.userService.logoutAccount().then(
      (val) => {
          obj.configService.logoutSessionLocal();
      } , 
      (err) => {
          obj.configService.logoutSessionLocal();
      });  
  }
  
}
