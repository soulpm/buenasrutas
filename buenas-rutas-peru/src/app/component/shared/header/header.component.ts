import { Component, OnInit, Input } from '@angular/core';
import { IMenu, IResultResponse } from 'src/app/models/config.model';
import { IUserModel } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';
import { ConfigService } from 'src/app/services/config.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {  
  imageCompany:string;
  titleHeader:string;
  menuList:IMenu[];
  @Input('parentObj') parent:any;
  
  userAccount:IUserModel = {}; 
  constructor(
    private configService: ConfigService,
    private userService  : UserService
  ) 
  { }

  ngOnInit() {
    let obj = this;
    obj.titleHeader = "Buenas Rutas Perú";
    obj.imageCompany = environment.pathImage+"/logo_1.jpg";
    obj.loadMenu();
  }
  selectMenu(menu:IMenu):void{
    let obj = this;
    obj.parent.menuSelect  = menu;
    if(obj.parent.menuSelect!=null){
      obj.configService.getSubMenuList(obj.parent.menuSelect.idMenu).then(
        (val) => {
          let objResponse:IResultResponse = val;
            switch(objResponse.responseCode){
                  case 200:
                  if(objResponse.data!=""){
                    obj.parent.subMenuList = objResponse.data;
                  }
                  else{
                    obj.parent.subMenuList = [];
                  }
              break;
              case -900:
                  obj.configService.showModalExpiration();  
              break;
            } 
        } , 
        (err) => {
            obj.configService.showAlertMessage("SubMenu Usuario","Ocurrio un error inesperado","alert-danger");
        });  
    }
  }
  loadMenu():void{
     let obj = this;
     obj.configService.getMenuList().then(
      (val) => {
        let objResponse:IResultResponse = val;
          switch(objResponse.responseCode){
                case 200:
                obj.menuList = objResponse.data;
            break;
            case -900:
                obj.configService.showModalExpiration();  
            break;
          } 
      } , 
      (err) => {
          obj.configService.showAlertMessage("Menu Usuario","Ocurrio un error inesperado","alert-danger");
      });
      obj.userService.getAccountUser().then(
        (val) => {
          let objResponse:IResultResponse = val;
            switch(objResponse.responseCode){
                  case 200:
                  obj.userAccount = objResponse.data;
              break;
              case -900:
                  obj.configService.showModalExpiration();  
              break;
            } 
        } , 
        (err) => {
            obj.configService.showAlertMessage("Cuenta Usuario","Ocurrio un error inesperado","alert-danger");
      });
      /*obj.menuList = [
        { idMenu:1,active:"active",nameOption:"Inicio",urlLink:"/init",iconOption:"fa fa-home"},
        { idMenu:2,active:"",nameOption:"Usuarios",urlLink:"/init/users",iconOption:""},
        { idMenu:3,active:"",nameOption:"Ventas",urlLink:"/init/sales"},
        { idMenu:4,active:"",nameOption:"Compras",urlLink:"/init/purchases"},
        { idMenu:5,active:"",nameOption:"Productos",urlLink:"/init/logistic"},
        { idMenu:6,active:"",nameOption:"Cajas",urlLink:"/init/boxes"},
        { idMenu:7,active:"",nameOption:"Reportes",urlLink:"/init/reports"}
     ];
     obj.userAccount = {
      userId:1,
      names:"Administrador" 
     };*/
     

  }

  logout():void{
    let obj = this;
    obj.userService.logoutAccount().then(
      (val) => {
          /*let objResponse:IResultResponse = val;
          switch(objResponse.responseCode){
                case 200:
                obj.userAccount = objResponse.data;
            break;
            case -900:
                obj.configService.showModalExpiration();  
            break;
          } */
          obj.configService.logoutSessionLocal();
      } , 
      (err) => {
          //obj.configService.showAlertMessage("Cuenta Usuario","Ocurrio un error inesperado","alert-danger");
          obj.configService.logoutSessionLocal();
      });
  }
}
