import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MessageSystemComponent } from './message-system/message-system.component';
import { ExpirationModuleComponent } from './expiration-module/expiration-module.component';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule
    ],
    declarations: [
        HeaderComponent,
        PageNotFoundComponent,
        MessageSystemComponent,
        ExpirationModuleComponent,
        SidebarComponent,
    ],
    exports: [
        HeaderComponent,
        ExpirationModuleComponent,
        PageNotFoundComponent,
        SidebarComponent
    ]
})
export class SharedModule { }
