import { Component, OnInit, Input } from '@angular/core';
import { IMenu, ISubMenu } from 'src/app/models/config.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input('parentObj') parent:any;
  constructor() { }

  ngOnInit() {
  }

}
