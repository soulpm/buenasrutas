import { IColumnTable } from "../models/config.model";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class ColumnTables  {

    getUserColumns():IColumnTable[]{
        let listUsersColumns = [
            {nameColumn: "Perfil"         , class:"ui-p-1 ui-column" },
            {nameColumn: "Tipo Documento" , class:"ui-p-2 ui-column"},
            {nameColumn: "N° Documento"   , class:"ui-p-3 ui-column"},
            {nameColumn: "Nombre Usuario" , class:"ui-p-4 ui-column"},
            {nameColumn: "Email"          , class:"ui-p-5 ui-column"},
            {nameColumn: "Estado"         , class:"ui-p-6 ui-column"},
            {nameColumn: "Foto"           , class:"ui-p-7 ui-column"},
            {nameColumn: "Opciones"       , class:"ui-p-8 ui-column"}
        ];
        return listUsersColumns;
    }

    
}
  

