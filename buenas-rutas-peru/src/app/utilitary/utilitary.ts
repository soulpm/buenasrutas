import { IMessageModel } from "../models/config.model";

export class Utilitary  {
    
    private messageModal:IMessageModel = {};

      toInteger(value: any): number {
        return parseInt(`${value}`, 0);
      }
      
      toString(value: any): string {
        return (value !== undefined && value !== null) ? `${value}` : '';
      }
      
      getValueInRange(value: number, max: number, min = 0): number {
        return Math.max(Math.min(value, max), min);
      }
      
      isString(value: any): value is string {
        return typeof value === 'string';
      }
      
      isNumber(value: any): value is number {
        return !isNaN(this.toInteger(value));
      }
      
      isInteger(value: any): value is number {
        return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
      }
      
      isDefined(value: any): boolean {
        return value !== undefined && value !== null;
      }
      
      padNumber(value: number) {
        if (this.isNumber(value)) {
          return `0${value}`.slice(-2);
        } else {
          return '';
        }
      }
      
      regExpEscape(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      }
      createCookie(name, value, days) {
        var expires;
        if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
          expires = "; expires=" + date.toDateString();
        } else {
         expires = "";
        }
        document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
      }
}
  