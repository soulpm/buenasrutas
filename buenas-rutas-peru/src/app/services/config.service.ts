import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { IMessageModel, IResultResponse } from '../models/config.model';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
declare var $:$;

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

    private messageSystem:IMessageModel              = {};
    private UrlBase:string            = environment.urlApiRest;
    private UrlListMenu:string        = this.UrlBase+"/config/ws_menu.php";
    private UrlListSubMenu:string     = this.UrlBase+"/config/ws_sub_menu.php";
    constructor(private http:HttpClient) { }

    getMenuList():any{
      let obj = this;
      let header = obj.getHeader();
      let parameters = new HttpParams();
      parameters = parameters.set("token_session",sessionStorage.getItem("user_session"));
      return new Promise((resolve,reject)=> {
        obj.http.post(obj.UrlListMenu,parameters,{headers:header}).subscribe(
            response => {
              resolve(response);
            },
            error => {
              let result:IResultResponse = {
                responseCode:-100,
                data: "Ocurrio un error, "+error
              }; 
              reject(result);
            })
       });
    }
    getSubMenuList(menu:number):any{
      let obj = this;
      let header = obj.getHeader();
      let parameters = new HttpParams();
      parameters = parameters.set("token_session",sessionStorage.getItem("user_session"));
      parameters = parameters.set("menu",menu.toString());
      return new Promise((resolve,reject)=> {
        obj.http.post(obj.UrlListSubMenu,parameters,{headers:header}).subscribe(
            response => {
              resolve(response);
            },
            error => {
              let result:IResultResponse = {
                responseCode:-100,
                data: "Ocurrio un error, "+error
              }; 
              reject(result);
            })
       });
    }
    
    logoutSessionLocal():void{
        window.localStorage.removeItem("user_session");
        window.location.href="/";
    }
    getHeader():HttpHeaders{
      let header = new HttpHeaders();
      let auth = "Basic " + btoa("DB440D77B3DEDE7E97BAB38C690CCD7E791D15DA"+":"+"891DFBE7C25B0B8212494AEF85A6DA0A9886C132");
      //header = header.append('Api-User-Agent', 'Example/1.0');
      //header = header.append('Authorization',auth);
      //header = header.append('Content-Type','application/json');
      //header = header.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      //header = header.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      //console.log("header: "+JSON.stringify(header));
      return header;
   }
   showAlertMessage(title:string,message:string,typeAlert:string):void{
      $("#container-message-system").css("display","block");
      $("#container-message-system").addClass(typeAlert);
      $("#title-message-system").html(title);
      $("#content-message-system").html(message);
      setTimeout(function(){
        $("#container-message-system").css("display","none");
      },6000);      
    }
    showModalExpiration():void{
      $("#modalExpiration").modal('show');
      $("#modalExpiration").on('shown.bs.modal', function() {
        
      });      
    }
}
