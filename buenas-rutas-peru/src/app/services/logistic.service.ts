import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigService } from './config.service';
import { IFilterProductModel } from '../models/producto.model';
import { IResultResponse } from '../models/config.model';

@Injectable({
  providedIn: 'root'
})
export class LogisticService {

  private UrlBase:string            = environment.urlApiRest;
  private UrlListProduct:string     = this.UrlBase+"/services/product/ws_list_product.php";
  private UrlNewProduct:string      = this.UrlBase+"/services/product/ws_list_product.php";
  private UrlUpdateProduct:string   = this.UrlBase+"/services/product/ws_list_product.php";
  private UrlDeleteProduct:string   = this.UrlBase+"/services/product/ws_list_product.php";
  private UrlInsertWarehouse:string = this.UrlBase+"/logistic/warehouse/ws_new_warehouse.php";
  private UrlUpdateWarehouse:string = this.UrlBase+"/logistic/warehouse/ws_edit_warehouse.php";
  private UrlListWarehouse:string   = this.UrlBase+"/logistic/warehouse/ws_list_warehouse.php";
  
  constructor(private http:HttpClient,
    private configService:ConfigService) { 
  }

  listProduct(filter:IFilterProductModel):any{
      let obj = this;
      let header = obj.configService.getHeader();
      let parameters = new HttpParams();
      parameters = parameters.set("token_session",sessionStorage.getItem("user_session"));
      parameters = parameters.set("proveedor",filter.proveedor.id);
      //parameters = parameters.set("familia",filter.familia.id);
      
      return new Promise((resolve,reject)=> {
        obj.http.post(obj.UrlListProduct,parameters,{headers:header}).subscribe(
            response => {
              let result:IResultResponse = response; 
              resolve(result.data);
            },
            error => {
              let result:IResultResponse = {
                responseCode:-100,
                data: "Ocurrio un error, "+error
              }; 
              reject(result);
            }
        )
      });
  }



}
