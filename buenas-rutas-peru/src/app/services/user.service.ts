import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ILoginResponse, IUserAccount, IUserFilterModel, IUserModel, IRoleModel } from '../models/user.model';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { ConfigService } from './config.service';
import { IResultResponse, IDropDownModel } from '../models/config.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private UrlLoginUser:string         = environment.urlApiRest+"/user/ws_login.php";
  private UrlLogout:string            = environment.urlApiRest+"/user/ws_logout.php";
  private UrlAccountUser:string       = environment.urlApiRest+"/user/ws_account.php";
  private UrlListUserFilter:string    = environment.urlApiRest+"/user/ws_filter_user.php";
  private UrlListUser:string          = environment.urlApiRest+"/user/ws_list_user.php";
  private UrlCreatUser:string         = environment.urlApiRest+"/user/ws_create_user.php";
  private UrlEditUser:string          = environment.urlApiRest+"/user/ws_edit_user.php";
  private UrlChangeCredential:string  = environment.urlApiRest+"/user/ws_change_credential.php";
  private UrlListRole:string          = environment.urlApiRest+"/user/ws_list_role.php";
  constructor(
    private http:HttpClient,
    private configService:ConfigService) {   
  }

  loginAccount(loginModel:ILoginResponse):any{
      let obj = this;
      return new Promise((resolve, reject) => {
        let loginResponse:ILoginResponse = {
            isLoginCorrect : false,
            message        : "Datos no válidos",
            stateLogin     : 0,
            userAccount    : {}
        };
        let header = obj.configService.getHeader();
        let parameters = new HttpParams();
        parameters = parameters.set('usr', loginModel.userId);
        parameters = parameters.set('pwd', loginModel.password);
        loginResponse.showMessage          = true;
        loginResponse.typeMessage          = "alert-warning";
        loginResponse.titleMessage         = "Error!";
        obj.http.post(obj.UrlLoginUser,parameters,{headers:header}).subscribe(
            data => {
              let result:IResultResponse = data; 
              if(result.responseCode == 200){
                sessionStorage.setItem("user_session",result.data);
                loginResponse.isLoginCorrect       = true;
                loginResponse.typeMessage          = "alert-success";
                loginResponse.titleMessage         = "";
                loginResponse.message              = "Bienvenid@ al Sistema";
                resolve(loginResponse);
              } 
              else{
                loginResponse.isLoginCorrect       = false;
                loginResponse.message              = result.data;
                reject(loginResponse);
              }
            },
            exception => {
                let result:IResultResponse = exception.error; 
                loginResponse.message      = result.data;  
                reject(loginResponse);
            }
        )
      });
   }

  logoutAccount():any{
     return new Promise((resolve, reject) => {
            let obj = this;
            let header = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('user_session',sessionStorage.getItem("user_session"));
            this.http.post(this.UrlLogout,parameters,{headers:header}).subscribe(
                response => {
                  /*let obj:IResultResponse = response;
                  let message = obj.data;*/
                  resolve(response); 
                },
                error => {
                  reject(error);
                }
            )
     }); 
  }
  getListRole(tokenSession:string):any{
      return new Promise((resolve, reject) => {
            let obj    = this;
            let header = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('type', "list_role");
            parameters = parameters.set('token_session',tokenSession);
            this.http.post(obj.UrlListRole,parameters,{headers:header}).subscribe(
                response => {
                  resolve(response); 
                },
                error => {
                  reject(error);
                }
            )
      });
  }
  getAccountUser():any{
      return new Promise((resolve, reject) => {
            let obj     = this;
            let header  = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('type', "account");
            let tokenSession:string =  sessionStorage.getItem("user_session");
            parameters = parameters.set('user_session',tokenSession);
            obj.http.post(this.UrlAccountUser,parameters,{headers:header}).subscribe(
                response => {
                  /*
                  let obj:IResultResponse = response;
                  let userAccount:IUserAccount = obj.data;
                  if(userAccount != null && userAccount != ""){
                      resolve(userAccount);
                  } */
                  resolve(response);
                },    
                error => {
                  reject(error);
                }
            )
      });
   }
  getListFilterUser(tokenSession:string):any{
      return new Promise((resolve, reject) => {
            let obj    = this;
            let header = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('user_session',tokenSession);
            this.http.post(this.UrlListUserFilter,parameters,{headers:header}).subscribe(
                response => {
                  resolve(response);
                },
                error => {
                  reject(error);
                }
            )
      });
  }   

  getListUser(userFilter:IUserFilterModel):any{
      return new Promise((resolve, reject) => {
            let obj    = this;
            let header = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('token_session',userFilter.token_session);
            parameters = parameters.set('id',userFilter.id.toString());
            parameters = parameters.set('name',userFilter.name);
            parameters = parameters.set('role',userFilter.role.toString());
            parameters = parameters.set('type_doc',userFilter.typeDocument.toString());
            parameters = parameters.set('state',userFilter.state.toString());
            this.http.post(this.UrlListUser,parameters,{headers:header}).subscribe(
                response => {
                  resolve(response);
                },
                error => {
                  reject(error);
                }
            )
      });
  }   

  createUser(userEntity:IUserModel,image:number,w_owner:number):any{
      return new Promise((resolve, reject) => {
            let obj     = this;
            let header  = obj.configService.getHeader();
            let parameters = new HttpParams();
            console.log("user: "+JSON.stringify(userEntity));
            parameters = parameters.set('type', "create");
            parameters = parameters.set('token_session', userEntity.tokenSession);
            parameters = parameters.set('role',userEntity.role.idRole.toString());
            parameters = parameters.set('names', userEntity.names);
            parameters = parameters.set('email', userEntity.email);
            parameters = parameters.set('type_doc', userEntity.typeNif.idDocument.toString());
            parameters = parameters.set('nif',userEntity.numberNif);
            parameters = parameters.set('sex',userEntity.sex.toString());
            parameters = parameters.set('address',userEntity.address);
            parameters = parameters.set('movilNumber',userEntity.movilNumber);
            parameters = parameters.set('landLine',userEntity.landLine);
            parameters = parameters.set('image',image.toString());
            parameters = parameters.set('w_owner',w_owner.toString());
            console.log("datos: "+JSON.stringify(userEntity)+" ; "+w_owner);
            this.http.post(obj.UrlCreatUser,parameters,{headers:header}).subscribe(
                response => {
                  let obj:IResultResponse = response;
                  let message = obj.data;
                      resolve(message); 
                },
                error => {
                  reject(error);
                }
            )
      });
  }
  editUser(userEntity:IUserModel,image:number):any{
      return new Promise((resolve, reject) => {
            let obj     = this;
            let header  = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('type', "edit");
            parameters = parameters.set('token_session', userEntity.tokenSession);
            parameters = parameters.set('user_id',userEntity.userId.toString());
            parameters = parameters.set('role',userEntity.role.idRole.toString());
            parameters = parameters.set('names', userEntity.names);
            parameters = parameters.set('email', userEntity.email);
            parameters = parameters.set('type_doc', userEntity.typeNif.idDocument.toString());
            parameters = parameters.set('nif',userEntity.numberNif);
            parameters = parameters.set('sex',userEntity.sex.toString());
            parameters = parameters.set('address',userEntity.address);
            parameters = parameters.set('movilNumber',userEntity.movilNumber);
            parameters = parameters.set('landLine',userEntity.landLine);
            parameters = parameters.set('image',image.toString());
            parameters = parameters.set('state',userEntity.estate.idState.toString());
            this.http.post(obj.UrlEditUser,parameters,{headers:header}).subscribe(
                response => {
                  let obj:IResultResponse = response;
                  let message = obj.data;
                      resolve(message); 
                },
                error => {
                  reject(error);
                }
            )
      });
  }
  changeCredential(token:string,userId:number,id:string,password:string):any{
      return new Promise((resolve, reject) => {
            let obj     = this;
            let header  = obj.configService.getHeader();
            let parameters = new HttpParams();
            parameters = parameters.set('token_session', token);
            parameters = parameters.set('id_user',userId.toString());
            parameters = parameters.set('id',id);
            parameters = parameters.set('password',password);
            this.http.post(obj.UrlChangeCredential,parameters,{headers:header}).subscribe(
                response => {
                  let obj:IResultResponse = response;
                  let message = obj.data;
                      resolve(message); 
                },
                error => {
                  reject(error);
                }
            )
      });
  }
    


}
