export const environment = {
  production: true,
  pathImage: "/assets/images/",
  initPage:"/init",
  urlApiRest: "http://buenasrutasperu.com/ws-buenas-rutas/services"
};
