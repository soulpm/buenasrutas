<?php
	header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:Content-Type");
	header("Access-Control-Allow-Methods:Get,Post");
	require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/services/utilities/helper_service.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/controller/controller_menu.php");
	
	validatePostParameters(array("role","user_session"));
	$role  		= $_REQUEST["role"];
	$p_token	= $_REQUEST["user_session"];	
	try{	
		$ctrl 		= new ControllerMenu();
		$listMenu 	=  $ctrl->listMenu($role,$p_token);
		if(isset($listMenu)){
			setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_OK,$listMenu),"POST");
		}
		else{
			setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_EXPIRE,$ctrl->MESSAGE_TRANSACTION),"POST");
		}
	}
	catch(Exception $e)
	{
		setResponse(formatResponse(VarConstantsBuenasRutas::_ERROR_VALIDATE_DATA_DB,$ctrl->MESSAGE_TRANSACTION." , ".$e->getMessage()),"POST");
	}
	

?>