<?php

    validatePostParameters(array("token_session","proveedor","familia","almacen"));
    $pToken			= $_REQUEST["token_session"];
    try{
        $ctrl 		= new ControllerUser();
        $listRole  =  $ctrl->getListRol($pToken);
        setResponse(formatResponse(VarConstantsMaskotaWeb::_CODE_TRANSACTION_OK,$listRole),"POST");
    }
    catch(Exception $e)
    {
        setResponse(formatResponse(VarConstantsMaskotaWeb::_ERROR_VALIDATE_DATA_DB,$ctrl->MESSAGE_TRANSACTION." , ".$e->getMessage()),"POST");
    }

?>