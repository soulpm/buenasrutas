<?php
    header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:Content-Type");
	header("Access-Control-Allow-Methods:Get,Post");
	
    require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
    require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/services/utilities/helper_service.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/controller/controller_user.php");
	
    validatePostParameters(array("usr","pwd"));
	$user 		= sha1($_REQUEST["usr"]);
	$password 	= sha1($_REQUEST["pwd"]);
	$ctrl 		= new ControllerUser();
	$userOnSign =  $ctrl->signIn($user,$password);
	
	if($ctrl->MESSAGE_TRANSACTION == "OK")		
	{
		setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_OK,$userOnSign),"POST");
	}
	else{setResponse(formatResponse(VarConstantsBuenasRutas::_ERROR_VALIDATE_DATA_DB,$ctrl->MESSAGE_TRANSACTION),"POST");}
    
?>