<?php
    header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:Content-Type");
	header("Access-Control-Allow-Methods:Get,Post");
	
    require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
    require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/services/utilities/helper_service.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/controller/controller_user.php");
	
    validatePostParameters(array("user_session","role","names","email","type_doc","nif","sex","address","movilNumber","landLine","image"));
    $pToken			= $_REQUEST["user_session"];
    $pRole 			= $_REQUEST["role"];
    $pNames 		= $_REQUEST["names"];
    $pEmail 		= $_REQUEST["email"];
    $pTypeDoc 		= $_REQUEST["type_doc"];
    $pNif 			= $_REQUEST["nif"];
    $pSex 			= $_REQUEST["sex"];
    $pAddress		= $_REQUEST["address"];
    $pMovilNumber	= $_REQUEST["movilNumber"];
    $pLandLine		= $_REQUEST["landLine"];
    $pImage 		= $_REQUEST["image"];
    try{
        $ctrl 		= new ControllerUser();
        $usr		= new EntityUser();
        $usr->tokenSession			= $pToken;
        $usr->role->roleId 			= $pRole;
        $usr->names					= $pNames;
        $usr->email					= $pEmail;
        $usr->typeNif->idDocument 	= $pTypeDoc;
        $usr->nif					= $pNif;
        $usr->sex					= $pSex;
        $usr->address				= $pAddress;
        $usr->movilNumber			= $pMovilNumber;
        $usr->landLine				= $pLandLine;
        $result  	=  $ctrl->create($usr,$pImage);
        if($result != -1){
            //grabar imagen con nombre obtenido
            // $ctrl->IMAGE_CREATE_VALUE;
        }
        setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_OK,$ctrl->MESSAGE_TRANSACTION),"POST");
    }
    catch(Exception $e)
    {
        setResponse(formatResponse(VarConstantsBuenasRutas::_ERROR_VALIDATE_DATA_DB,$ctrl->MESSAGE_TRANSACTION." , ".$e->getMessage()),"POST");
    }

?>