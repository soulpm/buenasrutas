<?php
	header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:Content-Type");
	header("Access-Control-Allow-Methods:Get,Post");
	
    require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
    require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/services/utilities/helper_service.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/controller/controller_user.php");
	
    validatePostParameters(array("user_session"));
	$pToken			= $_REQUEST["user_session"];
	$ctrl 			= new ControllerUser();
	$userAccount 	= $ctrl->userAccount($pToken);
	if(isset($userAccount)){
		setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_OK,$userAccount),"POST");
	}
	else if($ctrl->MESSAGE_TRANSACTION!="") {
		setResponse(formatResponse(VarConstantsBuenasRutas::_CODE_TRANSACTION_EXPIRE,$ctrl->MESSAGE_TRANSACTION),"POST");
	}
	else{
		setResponse(formatResponse(VarConstantsBuenasRutas::_ERROR_VALIDATE_DATA_DB,""),"POST");
	}


?>


