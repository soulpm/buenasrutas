<?php
	require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/dao/dao_user.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_user.php");
		
	class ControllerUser
	{
		public $IMAGE_CREATE_VALUE 		= "";
        public $MESSAGE_TRANSACTION     = "";
		
		//--------------------------------------------------------------------------------------
		// LOGIN USER
		//--------------------------------------------------------------------------------------
		public function signIn($user,$password)
		{
			$valReturn  				= null;
			$this->MESSAGE_TRANSACTION 	= "";
			$dao_user = new DAOUser();
			$request_user = $dao_user->signIn($user,$password);
            $this->MESSAGE_TRANSACTION = $dao_user->MESSAGE_TRANSACTION;
			if($this->MESSAGE_TRANSACTION!="")
			{
				if(gettype($request_user)!="NULL")
				{
					$valReturn = $request_user;
				}	
			}
			return $valReturn;
		}
		//----------------------------------------------------------------------------------------
		// SIGN OUT 
		//----------------------------------------------------------------------------------------
		public function signOut($ptoken)
		{
			$dao_user = new DAOUser();
			$result =  $dao_user->signOut($ptoken);
			return $result;
		}
		
		//--------------------------------------------------------------------------------------
		// ACCOUNT USER LOGIN
		//--------------------------------------------------------------------------------------
		public function userAccount($token)
		{
			$valReturn  				= null;
			$this->MESSAGE_TRANSACTION 	= "";
			$dao_user = new DAOUser();
			$request_user = $dao_user->userAccount($token);
			$this->MESSAGE_TRANSACTION = $dao_user->MESSAGE_TRANSACTION;
			return $request_user;
		}
		
		//--------------------------------------------------------------------------------------
		// LIST ROL
		//--------------------------------------------------------------------------------------
		public function getListRol($pToken)
		{
			$dao_user = new DAOUser();
			$data = $dao_user->getListRol($pToken);
			if(count($data)>0){
				return $data;
			}			
		}

		//--------------------------------------------------------------------------------------
		// LIST USERS
		//--------------------------------------------------------------------------------------
		public function getListUsers($pToken,$pId,$pName,$pRole,$pTypeDoc,$pState)
		{
			$dao_user = new DAOUser();
			$data = $dao_user->getListUsers($pToken,$pId,$pName,$pRole,$pTypeDoc,$pState);
			if(count($data)>0){
				return $data;
			}			
		}

		//--------------------------------------------------------------------------------------
		// REGISTER NEW USER
		//--------------------------------------------------------------------------------------
		public function create($usr,$img)
		{
			$this->MESSAGE_TRANSACTION ="";
			$this->IMAGE_CREATE_VALUE = "";
            $dao_user = new DAOUser();
			$result = $dao_user->Create($usr,$img);
			$this->IMAGE_CREATE_VALUE = $dao_user->IMAGE_CREATE_VALUE;
            $this->MESSAGE_TRANSACTION = $dao_user->MESSAGE_TRANSACTION;
			return $result;
		}
		//-------------------------------------------------------------------------------------------
		// UPDATE USER
		//-------------------------------------------------------------------------------------------
		public function update($usr,$img)
		{
			$this->MESSAGE_TRANSACTION ="";
			$this->IMAGE_CREATE_VALUE = "";
            $dao_user = new DAOUser();
			$result   = $dao_user->Update($usr,$img);
			$this->IMAGE_CREATE_VALUE  = $dao_user->IMAGE_CREATE_VALUE;
			$this->MESSAGE_TRANSACTION = $dao_user->MESSAGE_TRANSACTION;
            return $result;
		}
		//-------------------------------------------------------------------------------------------
		// CHANGE CREDENTIALS
		//-------------------------------------------------------------------------------------------
		public function updateCredential($iduser,$password)
		{
			$dao_user = new DAOUser();
			$result = $dao_user->UpdateCredential($iduser,$password);
			$this->MESSAGE_TRANSACTION = $dao_user->MESSAGE_TRANSACTION;
			return $result;
		}
		//--------------------------------------------------------------------------------------
		// LIST ROL
		//--------------------------------------------------------------------------------------
		public function getListUsersChild($pUser)
		{
			$dao_user = new DAOUser();
			$data = $dao_user->getListUsersChild($pUser);
			if(count($data)>0){
				return $data;
			}			
		}
		
		
	}

	
?>