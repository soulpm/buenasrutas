<?php
	require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/dao/dao_menu.php");
	class ControllerMenu	
	{
		public $MESSAGE_TRANSACTION     = "";
		
		public function listMenu($role,$p_token)
		{
			$dao_menu = new DAOMenu();
			$data = $dao_menu->getListMenu($role,$p_token);
			$this->MESSAGE_TRANSACTION = $dao_menu->MESSAGE_TRANSACTION;
			if(count($data)>0){
				return $data;
			}			
		}
	}
?>