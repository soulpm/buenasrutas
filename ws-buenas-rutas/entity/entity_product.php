<?php
    require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_user.php");
	

    class EntityProduct{
        public $idProduct;
        public $code;
        public $name;
        public $branch;
        public $proveedor;
        public $familia;
        public $almacen;
        public $ultimoPrecioCompra;
        public $precioPromedio;
        public $precio;
        public $precioTarjeta;
        public $stock;
        public $stockMinimo;
        public $stockDias;
        public $image;

        function __construct() {
            $this->branch       = new EntityBranch();
            $this->proveedor    = new EntityUser();
            $this->familia      = new EntityFamily();
            $this->almacen      = new EntityAlmacen();
            
		}
    }

    class EntityBranch{
        public $idBranch;
        public $Name;
    }

?>