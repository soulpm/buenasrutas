<?php
	require_once($_SERVER["DOCUMENT_ROOT"]."/constants-buenas-rutas.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/dao/dao.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_user.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_document.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_state.php");
	require_once($_SERVER["DOCUMENT_ROOT"].VarConstantsBuenasRutas::PATH_ROOT_APP."/entity/entity_role.php");
			
	class DAOUser 
	{
		const SP_LOGIN 					= "SP_USR_LOGIN";
		const SP_LOGOUT 				= "SP_USR_LOGOUT";
		const SP_ACCOUNT_USER			= "SP_USR_ACCOUNT";
		const SP_INSERT					= "SP_USR_INS";
		const SP_UPDATE 				= "SP_USR_UPD";
		const SP_LIST 					= "SP_USR_LST"	 ;
		const SP_LIST_ROL				= "SP_USR_ROL";
		const SP_LOGIN_APP 				= "SP_LOGIN_USR_APP";
		const SP_INSERT_USER_CHILD		= "SP_USR_INS_USR_CHILD";
		const SP_DELETE_USER_CHILD		= "SP_USR_DEL_USR_CHILD";
		const SP_LIST_USER_CHILD		= "SP_USR_LST_USR_CHILD";
		const SP_DELETE 				= "SP_DEL_USR";
		const SP_LIST_CLI 				= "SP_LST_USR_BY_CLI";
		const SP_CRED_USR 				= "SP_CREDENCIAL_USR";
		const SP_UPD_CREDENTIAL 		= "SP_USR_UPD_PWD";
		const SP_STATE 					= "SP_STD_USR";
		public $IMAGE_CREATE_VALUE      = "";
        public $MESSAGE_TRANSACTION     = "";
		
		//---------------------------------------------------------------------------------
		// LOGIN USER
		//---------------------------------------------------------------------------------
		public function signIn($usr,$pwd)
		{
			$dao = new Dao();
			$querie = "CALL ".DAOUser::SP_LOGIN."(?,?,@msg_db_transaccion)";
			$dao->prepareSP($querie);
			$dao->addParameter(1,$usr);
			$dao->addParameter(2,$pwd);
			/*$dao->addParameter(3,VarConstantsMaskotaWeb::DEVICE_PC);
			$dao->addParameter(4,$dao->getRealIP());*/
			if($dao->_ERROR_COMAND==""){	
				$dato = $dao->getDataTableSP();
				$this->MESSAGE_TRANSACTION  = $dao->getFirstValue("select @msg_db_transaccion",null);
				if(strpos($this->MESSAGE_TRANSACTION,'Error')=== false)
				{
					$data_ent = $dato[0];
					return $data_ent[0];
				}
			}
			else { $this->MESSAGE_TRANSACTION = VarConstantsMaskotaWeb::_ERROR_DB_TRANSACTION; }
		}
		//-------------------------------------------------------------------------------------- 
		// LOGOUT USER 
		//--------------------------------------------------------------------------------------
		public function signOut($ptoken)
		{
			$dao = new Dao();
			$querie = "CALL ".DAOUser::SP_LOGOUT."(?,@MSG_EXPIRE)";
			$dao->prepareSP($querie);
			$dao->addParameter(1,$ptoken);
			if($dao->_ERROR_COMAND==""){	
				$dao->execute();
				$result  = $dao->getFirstValue("select @MSG_EXPIRE",null);
				return utf8_encode($result);
			}
			else { $this->MESSAGE_TRANSACTION = 
			   $dao->formatMensajeError("Error, no se efectuo la operacion con la Base Datos");
			}
		}
		//---------------------------------------------------------------------------------
		// USER ACCOUNT DATA
		//---------------------------------------------------------------------------------
		public function userAccount($token)
		{
			$dao = new Dao();
			$querie = "CALL ".DAOUser::SP_ACCOUNT_USER."(?,@MSG_EXPIRE)";
			$dao->prepareSP($querie);
			$dao->addParameter(1,$token);
			if($dao->_ERROR_COMAND==""){	
				$dato = $dao->getDataTableSP();
				$entity = null;
				if($dato!=null){
					$data_ent = $dato[0];
					$entity = new EntityUser();			
					$entity->tokenSession		= $data_ent[0];
					$entity->role->idRole		= $data_ent[1];
					$entity->role->name			= $data_ent[2];
					$entity->names 	 			= utf8_encode($data_ent[3]);
					$entity->email	 			= $data_ent[4];
					$entity->typeNif 			= $data_ent[5];
					$entity->nif 				= $data_ent[6];
					$entity->photo 				= $data_ent[7];
				}
				$this->MESSAGE_TRANSACTION  = $dao->getFirstValue("select @MSG_EXPIRE",null);
				return $entity;
			}
			else { $this->MESSAGE_TRANSACTION = VarConstantsMaskotaWeb::_ERROR_DB_TRANSACTION; }
		}

		//------------------------------------------------------------------------------------
		// LIST ROL
		//------------------------------------------------------------------------------------
		public function getListRol($pToken)
		{
			$dao = new DAO();
			$querie = "CALL ".DAOUser::SP_LIST_ROL."(?)";
			$data =  $dao->getDataTable($querie,array($pToken));
			$array_rol = array();
			for($j=0;$j<count($data);$j++)
			{  
				$data_ent = $data[$j];
				$entity = new EntityRole();			
				$entity->idRole					= $data_ent[0];
				$entity->name					= $data_ent[1];
				$array_rol[$j] = $entity;
			}
			return $array_rol;
		}

		//------------------------------------------------------------------------------------
		// LIST USERS
		//------------------------------------------------------------------------------------
		public function getListUsers($pToken,$pId,$pName,$pRole,$pTypeDoc,$pState)
		{
			$dao = new DAO();
			$querie = "CALL ".DAOUser::SP_LIST."(?,?,?,?,?,?)";
			$data =  $dao->getDataTable($querie,array($pToken,$pId,$pName,$pRole,$pTypeDoc,$pState));
			$array_users = array();
			if($data!=null){
				for($j=0;$j<count($data);$j++)
				{  
					$data_ent = $data[$j];
					$entity = new EntityUser();			
					$entity->userId					= $data_ent[0];
					$entity->role 					= new EntityRole();
					$entity->role->idRole			= $data_ent[1];
					$entity->role->name				= $data_ent[2];
					$entity->typeNif		 	 	= new EntityDocument;
					$entity->typeNif->idDocument 	= $data_ent[3];
					$entity->typeNif->name 			= $data_ent[4];
					$entity->numberNif				= $data_ent[5];
					$entity->names  				= utf8_encode($data_ent[6]);
					$entity->email 	 				= $data_ent[7];
					$entity->estate					= new EntityState;
					$entity->estate->idState		= $data_ent[8];
					$entity->estate->name			= $data_ent[9];
					$entity->photo					= $data_ent[10];
					$array_users[$j] = $entity;
				}
			}
			return $array_users;
		}
		//---------------------------------------------------------------------------------------
		// INSERT USERS
		//---------------------------------------------------------------------------------------
		public function Create($usr,$img)
		{
			$dao = new DAO();
			$querie = "CALL ".DAOUser::SP_INSERT."(?,?,?,?,?,?,?,?,?,?,?,@img_db_name,@msg_db_transaccion)";
			$dao->prepareSP($querie);
			$dao->addParameter(1,$usr->tokenSession);
			$dao->addParameter(2,$usr->role->roleId);
			$dao->addParameter(3,$usr->names);
			$dao->addParameter(4,$usr->email);
			$dao->addParameter(5,$usr->typeNif->idDocument);
			$dao->addParameter(6,$usr->nif);
			$dao->addParameter(7,$usr->sex);
			$dao->addParameter(8,$usr->address);
			$dao->addParameter(9,$usr->movilNumber);
			$dao->addParameter(10,$usr->landLine);
			$dao->addParameter(11,$img);
			$result = $dao->execute();
			if($dao->_ERROR_COMAND!=""){$this->MESSAGE_TRANSACTION = $dao->formatMensajeError("Error, ");}
			else{
				$this->IMAGE_CREATE_VALUE   = $dao->getFirstValue("select @img_db_name",null);
				$this->MESSAGE_TRANSACTION  = $dao->getFirstValue("select @msg_db_transaccion",null);
            }
			return $result;
		}
		//--------------------------------------------------------------------------------------------
		// UPDATE USERS
		//-------------------------------------------------------------------------------------------
		public function Update($usr,$img)
		{
			$dao = new DAO();
			$querie = "CALL ".DAOUser::SP_UPDATE."(?,?,?,?,?,?,?,?,?,?,?,?,?,@img_db_name,@msg_db_transaccion)";
			$dao->prepareSP($querie);
			$dao->addParameter(1,$usr->tokenSession);
			$dao->addParameter(2,$usr->userId);
			$dao->addParameter(3,$usr->role->roleId);
			$dao->addParameter(4,$usr->names);
			$dao->addParameter(5,$usr->email);
			$dao->addParameter(6,$usr->typeNif->idDocument);
			$dao->addParameter(7,$usr->nif);
			$dao->addParameter(8,$usr->estate->idState);
			$dao->addParameter(9,$usr->sex);
			$dao->addParameter(10,$usr->address);
			$dao->addParameter(11,$usr->movilNumber);
			$dao->addParameter(12,$usr->landLine);
			$dao->addParameter(13,$img);
			$result = $dao->execute();
			if($dao->_ERROR_COMAND!=""){$this->MESSAGE_TRANSACTION = $dao->formatMensajeError("Error, ");}
			else{
				$this->IMAGE_CREATE_VALUE   = $dao->getFirstValue("select @img_db_name",null);
				$this->MESSAGE_TRANSACTION  = $dao->getFirstValue("select @msg_db_transaccion",null);
            }
			return $result;
		}
		//--------------------------------------------------------------------------------------------
		// UPDATE CREDENTIALS USERS
		//--------------------------------------------------------------------------------------------
		public function UpdateCredential($iduser,$newPassword)
		{
			$dao = new DAO();
			$querie = "CALL ".DAOUser::SP_UPD_CREDENTIAL."(?,?,@msg_db_transaccion)";
			$result = $dao->executeSP($querie,array($iduser,$newPassword));
			if($dao->_ERROR_COMAND!=""){$this->MESSAGE_TRANSACTION = $dao->formatMensajeError("Error, ");}
			else{
				$this->MESSAGE_TRANSACTION  = $dao->getFirstValue("select @msg_db_transaccion",null);
			}
			return $result;	
		}
		
	}
	/*
	$dao = new DAOUser();
	print_r($dao->getUsers('','','','','',''));
	$dao = new DAOUser();
	print_r($dao->getServerUser("USR00004",""));
	*/
?>